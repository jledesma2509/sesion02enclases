package com.academiamoviles.appsesion02

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //1. Evento Onclick
        btnEnviar.setOnClickListener {

            //2. Obtener la informacion de las cajas de texto
            val nombres = edtNombres.text.toString()
            val edad = edtEdad.text.toString()

            //3. Validando que los nombres no esten vacio
            if (nombres.isEmpty()){
                Toast.makeText(this,"Debe ingresar sus nombres",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            //4. Validando que la edad no este vacio
            if (edad.isEmpty()){
                Toast.makeText(this,"Debe ingrear su edad",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            
            //5. Validando que haya aceptado los terminos y condiciones
            if (!chkTerminos.isChecked){
                Toast.makeText(this,"Debe aceptar los terminos",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            var genero = if (rbMasculino.isChecked) "Masculino" else "Femenino"

            //6. Guardar informacion en el bundle
            val bundle = Bundle()
            bundle.putString("KEY_NOMBRES",nombres)
            bundle.putString("KEY_EDAD",edad)
            bundle.putString("KEY_GENERO",genero)


            //7. Pasar a la otra pantalla
            val intent = Intent(this,DestinoActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)

        }


    }

    fun sumar(numero1:Int, numero2:Int) : Int {

        return numero1 + numero2
    }

    fun sumar2(numero1:Int, numero2:Int) =  numero1 + numero2

    //Funciones Extension

    fun Int.sumar3( numero2:Int, numero3:Int) =  this + numero2 + numero3

    infix fun Int.mas( numero2:Int) =  this + numero2

    fun String.saludar() = "Hola $this"


}